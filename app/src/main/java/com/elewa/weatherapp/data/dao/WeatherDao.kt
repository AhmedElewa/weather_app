package com.elewa.weatherapp.data.dao

import androidx.room.*
import com.elewa.weatherapp.data.models.WeatherResponse

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWeather(weatherResponse: WeatherResponse): Long

    @Delete
    suspend fun deleteWeather(weatherResponse: WeatherResponse)

    @Query("DELETE FROM weather WHERE isCurrent = 1")
    suspend fun deleteCurrent()

    @Query("SELECT * FROM weather WHERE lat = :lat AND lon = :lon ")
    suspend fun getLocationWeather(lat: Double, lon: Double): WeatherResponse

}
