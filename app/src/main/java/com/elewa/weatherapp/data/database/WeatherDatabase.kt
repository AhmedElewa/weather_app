package com.elewa.weatherapp.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.elewa.weatherapp.data.dao.LocationDao
import com.elewa.weatherapp.data.dao.WeatherDao
import com.elewa.weatherapp.data.models.Location
import com.elewa.weatherapp.data.models.WeatherResponse
import com.elewa.weatherapp.util.Converters
import com.elewa.weatherapp.util.DATABASE_NAME

@Database(
    entities = [WeatherResponse::class, Location::class],
    version = 1,
    exportSchema = false
)

@TypeConverters(Converters::class)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
    abstract fun locationDao(): LocationDao

    companion object {
        @Volatile
        private var INSTANCE: WeatherDatabase? = null
        fun getInstance(context: Context): WeatherDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    WeatherDatabase::class.java,
                    DATABASE_NAME
                )
                    // Wipes and rebuilds instead of migrating if no Migration object.
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}
