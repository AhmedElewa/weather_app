package com.elewa.weatherapp.data.repository

import com.elewa.weatherapp.data.dao.LocationDao
import com.elewa.weatherapp.data.models.Location
import kotlinx.coroutines.flow.Flow

class LocationRepo(private val locationDao: LocationDao) {

    suspend fun insert(location: Location) {
        locationDao.insertLocation(location)
    }

    suspend fun delete(location: Location) {
        locationDao.deleteLocation(location)
    }

    fun getCurrentLocation(): Flow<Location> {
        return locationDao.getCurrentLocation()
    }

}
