package com.elewa.weatherapp.data.repository

import android.content.Context
import android.util.Log
import com.elewa.weatherapp.data.dao.WeatherDao
import com.elewa.weatherapp.data.models.WeatherResponse
import com.elewa.weatherapp.data.network.ApiClient
import com.elewa.weatherapp.data.network.WeatherService
import com.elewa.weatherapp.util.APP_ID
import retrofit2.HttpException
import java.io.IOException

class WeatherRepo(private val context: Context, private val weatherDao: WeatherDao) {

    private val retrofitService =
        ApiClient.getRetrofitInstance().create(WeatherService::class.java)

    suspend fun getCachedLocationWeather(lat: Double, lon: Double): WeatherResponse {
        return weatherDao.getLocationWeather(lat, lon)
    }


    suspend fun insert(weatherResponse: WeatherResponse): Long {
       return weatherDao.insertWeather(weatherResponse)
    }

    suspend fun delete(weatherResponse: WeatherResponse) {
        weatherDao.deleteWeather(weatherResponse)
    }

    suspend fun deleteCurrent() {
        weatherDao.deleteCurrent()
    }

    suspend fun getLive(
        lat: Double,
        lon: Double,
        units: String = "metric",
        lang: String = "en"
    ): ResponseState {
        try {
            val response = retrofitService.getWeather(
                lat,
                lon,
                "minutely",
                units,
                lang,
                APP_ID
            ).body()
            response
                ?.let { return ResponseState.Success(response) }
        } catch (e: IOException) {
            Log.e("Retrofit", "IOException, No Internet connection")
            return ResponseState.Error("No internet connection")
        } catch (e: HttpException) {
            Log.e("Retrofit", "HttpException, unexpected response")
            return ResponseState.Error("unexpected response")
        }
        return ResponseState.Empty
    }


    sealed class ResponseState {
        data class Success(val weatherResponse: WeatherResponse) : ResponseState()
        data class Error(val message: String) : ResponseState()
        object Empty : ResponseState()
    }
}

