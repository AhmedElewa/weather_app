package com.elewa.weatherapp.data.dao

import androidx.room.*
import com.elewa.weatherapp.data.models.Location
import kotlinx.coroutines.flow.Flow

@Dao
interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLocation(location: Location)

    @Delete
    suspend fun deleteLocation(location: Location)

    @Query("SELECT * From location WHERE id = 1")
    fun getCurrentLocation(): Flow<Location>

}
