package com.elewa.weatherapp

import android.app.Application
import com.elewa.weatherapp.data.database.WeatherDatabase
import com.elewa.weatherapp.data.repository.WeatherRepo
import com.elewa.weatherapp.data.repository.LocationRepo

class BasicApp : Application() {

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val database by lazy { WeatherDatabase.getInstance(this) }

    val WeatherRepo by lazy {
        WeatherRepo(this, database.weatherDao())
    }

    val LocationRepo by lazy {
        LocationRepo(
            database.locationDao(),
        )
    }


}