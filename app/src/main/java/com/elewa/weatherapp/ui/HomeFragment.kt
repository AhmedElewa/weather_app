package com.elewa.weatherapp.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.elewa.weatherapp.BasicApp
import com.elewa.weatherapp.R
import com.elewa.weatherapp.adapter.HourlyAdapter
import com.elewa.weatherapp.data.models.Location
import com.elewa.weatherapp.data.models.WeatherResponse
import com.elewa.weatherapp.data.repository.WeatherRepo
import com.elewa.weatherapp.databinding.FragmentHomeBinding
import com.elewa.weatherapp.util.PLACES_API_KEY
import com.elewa.weatherapp.util.SharedPrefsManager
import com.elewa.weatherapp.viewmodel.LocationViewModel
import com.elewa.weatherapp.viewmodel.LocationViewModelFactory
import com.elewa.weatherapp.viewmodel.WeatherViewModel
import com.elewa.weatherapp.viewmodel.WeatherViewModelFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.IOException
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


class HomeFragment : Fragment() {

    private val binding: FragmentHomeBinding by lazy { FragmentHomeBinding.inflate(layoutInflater) }
    private val weatherViewModel: WeatherViewModel by viewModels {
        WeatherViewModelFactory((requireActivity().application as BasicApp).WeatherRepo)
    }
    private val locationViewModel: LocationViewModel by viewModels {
        LocationViewModelFactory((requireActivity().application as BasicApp).LocationRepo)
    }
    private lateinit var geocoder: Geocoder
    private var addresses = mutableListOf<Address>()
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val locationPermissionCode = 2
    private var lon = 0.0
    private var lat = 0.0
    private var city = ""
    private lateinit var placesClient: PlacesClient
    private val prefs by lazy { SharedPrefsManager.newInstance(requireContext()) }
    private val AUTOCOMPLETE_REQUEST_CODE = 1


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        geocoder = Geocoder(context, Locale.getDefault())


        //places
        if (!Places.isInitialized()) {
            Places.initialize(requireActivity(), PLACES_API_KEY);
        }

        placesClient = Places.createClient(requireActivity())


        binding.txtSearch.setOnClickListener {
            val fields = listOf(Place.Field.LAT_LNG, Place.Field.NAME)

            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                .build(requireActivity())
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }


        lifecycleScope.launchWhenStarted {
            if (prefs.getBoolean("location",false)) {
                getCachedLocation()
            }else{
                prefs.putBoolean("location",true)
                locationViewModel.insert(
                    Location(
                        roundDouble(0.0),
                        roundDouble(0.0),
                        getString(R.string.unknown),
                        1
                    )
                )
                delay(1500)
                getCachedLocation()
            }
        }


        lifecycleScope.launchWhenStarted {
            delay(1000)
            if (prefs.getBoolean("location",false)) {
                getLocationFromGPS()
            }

        }



        binding.btnAllowPermission.setOnClickListener {
            getLocationFromGPS()

        }

        binding.imgGetCurrentLocation.setOnClickListener {
            getLocationFromGPS()
        }

        // SwipeRefresh
        binding.swipeRefresh.setProgressBackgroundColorSchemeColor(Color.parseColor("#FF313131"))
        binding.swipeRefresh.setColorSchemeColors(Color.parseColor("#ffeb3b"))
        binding.swipeRefresh.setOnRefreshListener {
            lifecycleScope.launchWhenStarted {
                if (lat != 0.0 && lon != 0.0) {
                    weatherViewModel.getLiveWeather(lat, lon)
                } else {
                    getLocationFromGPS()
                }
            }
        }

        binding.txtDate.text =
            SimpleDateFormat("E, dd MMM", Locale.getDefault()).format(System.currentTimeMillis())


        // Get Retrofit Response
        lifecycleScope.launchWhenStarted {
            weatherViewModel.weatherResponse.collect {
                when (it) {
                    is WeatherRepo.ResponseState.Success -> {
                        binding.swipeRefresh.isRefreshing = false
                        displayWeather(it.weatherResponse)
                        // Delete old current weather data
                        weatherViewModel.deleteCurrent()
                        val response = it.weatherResponse
                        response.isCurrent = true
                        // Insert the new current weather data
                        weatherViewModel.insert(response)

                    }
                    is WeatherRepo.ResponseState.Error -> {
                        if (lat != 0.0 && lon != 0.0) {
                            val weatherResponse =
                                weatherViewModel.getCachedLocationWeather(lat, lon)
                            if (weatherResponse != null) {
                                displayWeather(weatherResponse)
                            }
                        }
                        binding.swipeRefresh.isRefreshing = false
                        Snackbar.make(
                            binding.root,
                            getString(R.string.no_internet_connection),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                    else -> Unit
                }
                weatherViewModel.weatherResponse.value = WeatherRepo.ResponseState.Empty
            }
        }

        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvHourly.layoutManager = layoutManager
        binding.rvHourly.adapter = HourlyAdapter(requireContext())

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        locationViewModel.insert(
                            Location(
                                place.latLng!!.latitude,
                                place.latLng!!.longitude,
                                place.name!!,
                                1
                            )
                        )

                        lifecycleScope.launchWhenStarted {
                            delay(1000)
                            getCachedLocation()
                        }


                        binding.txtSearch.setText(place.name.toString())
//                        binding.txtCurrentAddress.setText(place.name.toString())
                        Log.i("here", "Place: ${place.name}, ${place.latLng}")
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        Snackbar.make(
                            binding.root,
                            getString(R.string.cant_get_this_location),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    // Display weather Data
    @SuppressLint("UseCompatLoadingForDrawables")
    private fun displayWeather(weatherResponse: WeatherResponse) {
        val current = weatherResponse.current
        var temp = current.temp

        binding.txtTemp.text = temp.roundToInt().toString()
        binding.txtDescription.text =
            current.weather[0].description.capitalize(Locale.ROOT)
        binding.txtWindSpeed.text =
            "${current.wind_speed} ${getString(R.string.m_s)}"
        binding.txtTempUnit.text = "°C"
        binding.txtPressure.text = "${current.pressure} ${getString(R.string.hpa)}"
        binding.txtHumidity.text = "${current.humidity} %"
        binding.txtClouds.text = "${current.clouds} %"
        binding.txtVisibility.text = "${current.visibility} ${getString(R.string.meter)}"

        // Display icon
        when (current.weather[0].main) {
            "Clear" -> {
                if (Calendar.getInstance().timeInMillis / 1000 in current.sunrise until current.sunset) {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.clear_day))
                } else {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.clear_night))
                }
            }
            "Clouds" -> {
                if (Calendar.getInstance().timeInMillis / 1000 in current.sunrise until current.sunset) {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.cloudy_day))
                } else {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.cloudy_night))
                }
            }
            "Drizzle" -> {
                if (Calendar.getInstance().timeInMillis / 1000 in current.sunrise until current.sunset) {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.rainy_day))
                } else {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.rainy_night))
                }
            }
            "Rain" -> {
                if (Calendar.getInstance().timeInMillis / 1000 in current.sunrise until current.sunset) {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.rainy_day))
                } else {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.rainy_night))
                }
            }
            "Snow" -> {
                binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.snow))
            }
            "Thunderstorm" -> {
                binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.storm))
            }
            else -> {
                if (Calendar.getInstance().timeInMillis / 1000 in current.sunrise until current.sunset) {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.foggy_day))
                } else {
                    binding.ivIcon.setImageDrawable(context?.getDrawable(R.drawable.foggy_night))
                }
            }
        }

        val hourlyAdapter = binding.rvHourly.adapter as HourlyAdapter

        // Send sunrise and sunset time to Adapter
        hourlyAdapter.setSunriseAndSunset(weatherResponse.daily)
        // Add List to Hourly Adapter
        hourlyAdapter.submitList(weatherResponse.hourly.subList(0, 24))

    }

    private suspend fun getCachedLocation() {

        locationViewModel.getCurrentLocation().collect {
            it.let {
                if (it.lat != 0.0 && it.lon != 0.0) {
                    val weatherResponse =
                        weatherViewModel.getCachedLocationWeather(it.lat, it.lon)
                    if (weatherResponse != null) {
                        displayWeather(weatherResponse)
                    }
                    binding.swipeRefresh.isRefreshing = true
                    weatherViewModel.getLiveWeather(
                        it.lat,
                        it.lon,
                    )
                }else{
                    getLocationFromGPS()
                }
                lat = it.lat
                lon = it.lon
                city = it.name
                binding.txtCurrentAddress.text = it.name

            }
        }

    }

    private fun getLocationFromGPS() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                locationPermissionCode
            )
            return
        }
        fusedLocationClient.getCurrentLocation(
            LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY,
            null
        ).addOnSuccessListener {
            if (it != null) {
                var newCity = ""
                try {
                    addresses = geocoder.getFromLocation(
                        it.latitude,
                        it.longitude,
                        1
                    )
                    newCity = if (addresses[0].locality.isNullOrEmpty()) {
                        addresses[0].adminArea
                    } else {
                        addresses[0].locality
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                if ((roundDouble(it.latitude) != lat && roundDouble(it.latitude) != lon) || newCity != city) {
                    locationViewModel.insert(
                        Location(
                            roundDouble(it.latitude),
                            roundDouble(it.longitude),
                            newCity,
                            1
                        )
                    )
                    weatherViewModel.getLiveWeather(it.latitude, it.latitude)
                    binding.txtSearch.setText("")
                }
            } else {
                Snackbar.make(binding.root, "Failed to get Location", Snackbar.LENGTH_SHORT)
                    .show()
            }
        }.addOnFailureListener {
            Snackbar.make(binding.root, "Failed to get location", Snackbar.LENGTH_SHORT)
                .show()
        }
    }


    private fun roundDouble(double: Double): Double {
        return BigDecimal(double).setScale(4, RoundingMode.HALF_UP).toDouble()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == locationPermissionCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                binding.currentTempLayout.visibility = View.VISIBLE
                binding.detailsLayout.visibility = View.VISIBLE
                binding.noPermissionLayout.visibility = View.GONE
                getLocationFromGPS()
            } else {
                binding.currentTempLayout.visibility = View.GONE
                binding.detailsLayout.visibility = View.GONE
                binding.noPermissionLayout.visibility = View.VISIBLE
            }
        }
    }


}