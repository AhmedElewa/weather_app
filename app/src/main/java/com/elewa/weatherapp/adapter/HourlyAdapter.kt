package com.elewa.weatherapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.elewa.weatherapp.R
import com.elewa.weatherapp.data.models.Daily
import com.elewa.weatherapp.data.models.Hourly
import com.elewa.weatherapp.databinding.HourlyItemBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


class HourlyAdapter(private val context: Context) :
    ListAdapter<Hourly, HourlyAdapter.HourlyViewHolder>(HourlyDiffUtil()) {
    private var tomorrowTime = 0
    private var todaySunrise = 0
    private var todaySunset = 0
    private var tomorrowSunrise = 0
    private var tomorrowSunset = 0
    private var sunrise = 0
    private var sunset = 0

    inner class HourlyViewHolder(val binding: HourlyItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourlyViewHolder {
        return HourlyViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.hourly_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: HourlyViewHolder, position: Int) {
        val formatter = SimpleDateFormat("h a", Locale.getDefault())
        val time = formatter.format(getItem(position).dt.toLong() * 1000)
        var temp = getItem(position).temp

        holder.binding.tvHourlyTempUnit.text = "°C"
        holder.binding.tvHourlyTemp.text = temp.roundToInt().toString()
        holder.binding.tvHourlyTime.text = time
        if (getItem(position).dt < tomorrowTime - 72000) {
            sunrise = todaySunrise
            sunset = todaySunset
        } else {
            sunrise = tomorrowSunrise
            sunset = tomorrowSunset
        }
        when (getItem(position).weather[0].main) {
            "Clear" -> {
                if (getItem(position).dt.toLong() in sunrise until sunset) {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.clear_day))
                } else {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.clear_night))
                }
            }
            "Clouds" -> {
                if (getItem(position).dt.toLong() in sunrise until sunset) {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.cloudy_day))
                } else {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.cloudy_night))
                }
            }
            "Drizzle" -> {
                if (getItem(position).dt.toLong() in sunrise until sunset) {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.rainy_day))
                } else {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.rainy_night))
                }
            }
            "Rain" -> {
                if (getItem(position).dt.toLong() in sunrise until sunset) {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.rainy_day))
                } else {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.rainy_night))
                }
            }
            "Snow" -> {
                holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.snow))
            }
            "Thunderstorm" -> {
                holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.storm))
            }
            else -> {
                if (getItem(position).dt.toLong() in sunrise until sunset) {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.foggy_day))
                } else {
                    holder.binding.ivHourlyIcon.setImageDrawable(context.getDrawable(R.drawable.foggy_night))
                }
            }
        }

    }

    fun setSunriseAndSunset(daily: List<Daily>) {
        todaySunrise = daily[0].sunrise
        todaySunset = daily[0].sunset
        tomorrowSunrise = daily[1].sunrise
        tomorrowSunset = daily[1].sunset
        tomorrowTime = daily[1].dt
    }
}

class HourlyDiffUtil : DiffUtil.ItemCallback<Hourly>() {
    override fun areItemsTheSame(oldItem: Hourly, newItem: Hourly): Boolean {
        return oldItem.dt == newItem.dt
    }

    override fun areContentsTheSame(oldItem: Hourly, newItem: Hourly): Boolean {
        return oldItem == newItem
    }

}
