package com.elewa.weatherapp.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.elewa.weatherapp.data.models.Location
import com.elewa.weatherapp.data.repository.LocationRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class LocationViewModel(private val locationRepo: LocationRepo) : ViewModel() {


    fun insert(location: Location) = viewModelScope.launch {
        locationRepo.insert(location)
    }

    fun delete(location: Location) = viewModelScope.launch {
        locationRepo.delete(location)
    }

    fun getCurrentLocation(): Flow<Location> {
        return locationRepo.getCurrentLocation()
    }



}

class LocationViewModelFactory(private val repository: LocationRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LocationViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return LocationViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
