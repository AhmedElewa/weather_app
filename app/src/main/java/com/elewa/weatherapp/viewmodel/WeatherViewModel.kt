package com.elewa.weatherapp.viewmodel

import androidx.lifecycle.*
import com.elewa.weatherapp.data.models.WeatherResponse
import com.elewa.weatherapp.data.repository.WeatherRepo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class WeatherViewModel(private val repo: WeatherRepo) : ViewModel() {

    private val _weatherResponse =
        MutableStateFlow<WeatherRepo.ResponseState>(WeatherRepo.ResponseState.Empty)

    val weatherResponse = _weatherResponse


    fun insert(weatherResponse: WeatherResponse): Long {
        var result: Long = 0;
        viewModelScope.launch {
            result = repo.insert(weatherResponse)
        }
        return result
    }

    fun delete(weatherResponse: WeatherResponse) = viewModelScope.launch {
        repo.delete(weatherResponse)
    }

    fun deleteCurrent() = viewModelScope.launch {
        repo.deleteCurrent()
    }

    suspend fun getCachedLocationWeather(lat: Double, lon: Double): WeatherResponse {
        return repo.getCachedLocationWeather(lat, lon)
    }




    fun getLiveWeather(
        lat: Double,
        lon: Double,
        units: String = "metric",
        lang: String = "en"
    ) {
        viewModelScope.launch {
            _weatherResponse.value = repo.getLive(lat, lon, units, lang)
        }
    }

}

class WeatherViewModelFactory(private val repository: WeatherRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WeatherViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return WeatherViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
