# Weather App #

Android mobile application with the ability to display current weather conditions to the current location using GPS,
 or by searching with cities name.

![Alt text](https://cdn1.bbcode0.com/uploads/2021/3/27/ac723213f44be32ae64a4f5ce6d8954b-full.jpg)
![Alt text](https://cdn1.bbcode0.com/uploads/2021/3/27/d523f7d9a4a7d23cdfbd7fdd8f8b5e64-full.jpg)

### Features ###

* Weather condition with full details.
* Get current weather conditions using GPS.
* Search with city name to get current weather conditions.


### Technologies Used ###

* Kotlin
* ViewModel Lifecycel
* Courotines
* Retrofit
* Room

### APIS ###

* Open Weather Map
* Google Places Api 


### Download Apk [here](https://drive.google.com/file/d/189hYmDe8EVbvlJcNiPUE0-JPigbT6dpm/view?usp=sharing) ###
